package com.demo.mvptestclient.routes;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class KafkaRoutes extends RouteBuilder {

    @Override
    public void configure() {

        from("direct:publish-to-hip")
                .setHeader(KafkaConstants.KEY, simple("${random(100000, 5000000)}", String.class))
                .log("Client: Received Decision with CorrelationID: ${headers[kafka.KEY]}")
                .log("Client: Sending the following decision payload to HIP: ${body}")
                .marshal().avro("com.demo.mvptestclient.Decision")
                .to("kafka:decision-topic?brokers=172.30.75.229:9092")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(202))
                .setBody(constant("Decision sent to GCMS!"));

        from("kafka:confirmation-topic?brokers=172.30.75.229:9092")
                .unmarshal().avro("com.demo.mvptestclient.Confirmation")
                .log("Client: Received conformation back from HIP ${body}");

    }

}
